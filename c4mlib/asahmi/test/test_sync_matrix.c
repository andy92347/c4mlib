/**
 * @file test_sync_matrix.c
 * @author LiYu87
 * @date 2019.07.02
 * @brief
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    ASA_STDIO_init();

    uint8_t data[2][3] = {{1, 2, 3}, {4, 5, 6}};

    HMI_snget_matrix(HMI_TYPE_I8, 2, 3, data);  // send data
    HMI_snput_matrix(HMI_TYPE_I8, 2, 3, data);  // send data

    return 0;
}
