// #define USE_C4MLIB_DEBUG
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
// 3bit control flag + 5bit address
void SPIS1_cb(void) {
    uint8_t tempData = 0;
    uint8_t cf = 0;
    uint8_t cf_addr = 0;
    switch (ASASPISerialIsrStr->sm_status) {
        case SPIS_STATE_IDLE:
            tempData = SPIS_Inst.read_byte();
            cf = (tempData & 0x07);
            cf_addr = tempData >> 3;  // 這裡測試使用3 bits control flag
            ASASPISerialIsrStr->reg_address = cf_addr;
            ASASPISerialIsrStr->sm_status = SPIS_STATE_DATA;
            ASASPISerialIsrStr->check_sum = 0;
            ASASPISerialIsrStr->byte_counter =
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .sz_reg -
                1;
            break;
        case SPIS_STATE_DATA:
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
            }
            break;
        default:
            break;
    }
    DEBUG_INFO("tempData:%x, CF:%x, CF_ADDR:%x, status:%d, byte_counter:%d\n",
               tempData, cf, cf_addr, ASASPISerialIsrStr->sm_status,
               ASASPISerialIsrStr->byte_counter);
}