// #define USE_C4MLIB_DEBUG
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
// Slave state machine
void SPIS0_cb(void) {
    uint8_t tempData = 0;
    switch (ASASPISerialIsrStr->sm_status) {
        case SPIS_STATE_IDLE:
            tempData = SPIS_Inst.read_byte();
            // Initialize ASASPISerialIsrStr structure.
            ASASPISerialIsrStr->reg_address = tempData & 0x7F;
            ASASPISerialIsrStr->check_sum = 0;
            SPIS_Inst.error_code = HAL_OK;
            ASASPISerialIsrStr->byte_counter = 0;
            // Write mode
            if (tempData & 0x80) {
                ASASPISerialIsrStr->sm_status =
                    SPIS_STATE_WRITE;  // master send slave get
                ASASPISerialIsrStr->rw_mode = 0;
                SPIS_Inst.write_byte(SPIS_Inst.read_byte());
            }
            // Read mode
            else {
                ASASPISerialIsrStr->sm_status =
                    SPIS_STATE_READ;  // master get slave send
                ASASPISerialIsrStr->rw_mode = 1;
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                ASASPISerialIsrStr->byte_counter++;
            }
            break;
        case SPIS_STATE_WRITE:
            if (ASASPISerialIsrStr->byte_counter <
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .sz_reg) {
                ASASPISerialIsrStr->temp[ASASPISerialIsrStr->byte_counter] =
                    SPIS_Inst.read_byte();
                ASASPISerialIsrStr->byte_counter++;
                ASASPISerialIsrStr->sm_status = SPIS_STATE_WRITE;
                SPIS_Inst.write_byte(SPIS_Inst.read_byte());
            }
            // 喚回最後一筆
            else if (ASASPISerialIsrStr->byte_counter ==
                     ASASPISerialIsrStr
                         ->remo_reg[ASASPISerialIsrStr->reg_address]
                         .sz_reg) {
                ASASPISerialIsrStr->byte_counter++;
                ASASPISerialIsrStr->sm_status = SPIS_STATE_WRITE;
                SPIS_Inst.write_byte(SPIS_Inst.read_byte());
            }
            // 檢查 Master端的檢查碼
            else if (ASASPISerialIsrStr->byte_counter ==
                     (ASASPISerialIsrStr
                          ->remo_reg[ASASPISerialIsrStr->reg_address]
                          .sz_reg +
                      1)) {
                SPIS_Inst.error_code = SPIS_Inst.read_byte();
                // Fail
                if (SPIS_Inst.error_code != HAL_OK) {
                    SPIS_Inst.write_byte(0xFF);
                }
                // Success
                else {
                    ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
                    SPIS_Inst.write_byte(0xAA);
                    // update the data if check is okay
                    for (uint8_t i = 0;
                         i < ASASPISerialIsrStr
                                 ->remo_reg[ASASPISerialIsrStr->reg_address]
                                 .sz_reg;
                         i++) {
                        ASASPISerialIsrStr
                            ->remo_reg[ASASPISerialIsrStr->reg_address]
                            .data_p[i] = ASASPISerialIsrStr->temp[i];
                    }
                }
            }
            break;
        case SPIS_STATE_READ:
            tempData = SPIS_Inst.read_byte();
            if (ASASPISerialIsrStr->byte_counter == 1) {
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                ASASPISerialIsrStr->byte_counter++;
            }
            else if (ASASPISerialIsrStr->byte_counter <
                     ASASPISerialIsrStr
                         ->remo_reg[ASASPISerialIsrStr->reg_address]
                         .sz_reg) {
                if (tempData !=
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter - 2]) {
                    SPIS_Inst.error_code = HAL_ERROR_CHK;
                }
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                ASASPISerialIsrStr->byte_counter++;
            }
            else if (ASASPISerialIsrStr->byte_counter ==
                     ASASPISerialIsrStr
                         ->remo_reg[ASASPISerialIsrStr->reg_address]
                         .sz_reg) {
                if (tempData !=
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter - 2]) {
                    SPIS_Inst.error_code = HAL_ERROR_CHK;
                }
                SPIS_Inst.write_byte(87);
                ASASPISerialIsrStr->byte_counter++;
            }
            else if (ASASPISerialIsrStr->byte_counter ==
                     (ASASPISerialIsrStr
                          ->remo_reg[ASASPISerialIsrStr->reg_address]
                          .sz_reg +
                      1)) {
                if (tempData !=
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter - 2]) {
                    SPIS_Inst.error_code = HAL_ERROR_CHK;
                }
                SPIS_Inst.write_byte(SPIS_Inst.error_code);
                ASASPISerialIsrStr->byte_counter++;
            }
            else if (ASASPISerialIsrStr->byte_counter ==
                     (ASASPISerialIsrStr
                          ->remo_reg[ASASPISerialIsrStr->reg_address]
                          .sz_reg +
                      2)) {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
                // Fail
                if (SPIS_Inst.error_code != HAL_OK) {
                    SPIS_Inst.write_byte(0xFF);
                }
                // Success
                else {
                    SPIS_Inst.write_byte(0xAA);
                }
            }
            break;

        default:
            break;
    }
    DEBUG_INFO("tempData:%d, state:%d, chk:%d\n", tempData,
               ASASPISerialIsrStr->sm_status, SPIS_Inst.error_code);
}
