#include "c4mlib/hardware/src/hal_uart.h"

#if defined(__AVR_ATmega128__)
#    include "m128/io.c"
//#  include "m128/uart.c"
#    include "m128/spi.c"
#    include "m128/twi.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/io.c"
//#  include "m88/uart.c"
#    include "m88/spi.c"
#    include "m88/twi.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/io.c"
//#  include "tiny2313/uart.c"
#    include "tiny2313/spi.c"
#    include "tiny2313/twi.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
