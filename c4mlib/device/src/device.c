#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/device/src/config_str.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/stdio/src/stdio.h"

/* public functions declaration section ------------------------------------- */
void ASA_DEVICE_set(void);

/* define section ----------------------------------------------------------- */

/* Global variables section ------------------------------------------------- */
TypeOfASAConfigStr ASAConfigStr_inst;

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
void ASA_DEVICE_set(void) {
    ASA_STDIO_init();
    ASABUS_ID_init();
    ASABUS_SPI_init();
    // NOTE: HAL UART initialize at RemoReg_init() function

    // Readback Configure in EEPROM, default start Address: 0
    EEPROM_get(0, sizeof(ASAConfigStr_inst), &ASAConfigStr_inst);
}
