/**
 * @file hal_uart.h
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Create a hardware abstraction layer to separate hardware dependent.
 *
 *
 */

#ifndef C4MLIB_HARDWARE_HAL_UART_H
#define C4MLIB_HARDWARE_HAL_UART_H

#define DEFAULTUARTBAUD 38400

#include <stdint.h>

typedef struct {
    void (*init)(void);
    void (*tx_compelete_cb)(void);
    void (*rx_compelete_cb)(void);
    void (*write_byte)(uint8_t data);
    void (*write_multi_bytes)(uint8_t* data_p, uint8_t sz_data);
    void (*read_byte)(uint8_t* data_p);
    void (*read_multi_bytes)(uint8_t* data_p, uint8_t sz_data);
    uint8_t* error_code;
} TypeOfUARTInst;

#if defined(__AVR_ATmega128__)
TypeOfUARTInst UART0_Inst;
TypeOfUARTInst UART1_Inst;
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
TypeOfUARTInst UART0_Inst;
#elif defined(__AVR_ATtiny2313__)
TypeOfUARTInst UART0_Inst;
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HARDWARE_HAL_UART_H
