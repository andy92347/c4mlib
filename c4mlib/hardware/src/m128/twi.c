#include "c4mlib/hardware/src/twi.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

// TODO: HARDWARE TWI 尋找負責人，或安排時程

/* public functions declaration section ------------------------------------- */
char TWI_fpt(char LSByte, char Mask, char Shift, char Data);
char TWI_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char TWI_put(char LSbyte, char Bytes, void *Data_p);
char TWI_get(char LSbyte, char Bytes, void *Data_p);
char TWI_set(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("TWI_fpt")));

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
char TWI_fpt(char LSByte, char Mask, char Shift, char Data) {
    switch (LSByte) {
        case 200:
            REGFPT(TWBR, Mask, Shift, Data);
            break;
        case 201:
            REGFPT(TWCR, Mask, Shift, Data);
            break;
        case 202:
            REGFPT(TWSR, Mask, Shift, Data);
            break;
        case 203:
            REGFPT(TWAR, Mask, Shift, Data);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char TWI_fgt(char LSByte, char Mask, char Shift, void *Data_p) {
    switch (LSByte) {
        case 202:
            REGFGT(TWSR, Mask, Shift, Data_p);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char TWI_put(char LSByte, char Bytes, void *Data_p) {
    switch (LSByte) {
        case 0: {
            if (Bytes == 1) {
                TWDR = *((uint8_t *)Data_p);
            }
            else {
                return RES_ERROR_BYTES;
            }
            break;
        }
        default: { return RES_ERROR_LSBYTE; }
    }
    return RES_OK;
}

char TWI_get(char LSByte, char Bytes, void *Data_p) {
    switch (LSByte) {
        case 0: {
            if (Bytes == 1) {
                *((uint8_t *)Data_p) = TWDR;
            }
            else {
                return RES_ERROR_BYTES;
            }
            break;
        }
        default: { return RES_ERROR_LSBYTE; }
    }
    return RES_OK;
}
