/**
 * @file asa_twi.h
 * @author Yuchen
 * @brief 放置TWI通訊封包函式原型
 * @date 2019-03-12
 */
#ifndef ASA_TWI_H
#define ASA_TWI_H
#include "c4mlib/macro/src/std_res.h"

#include "twi.h"
/* Public Section Start */
/**
 * @brief ASA TWI 送訊函式
 *
 * @param mode      TWI通訊模式:    目前支援:1, 2, 3, 4, 5
 * @param SLA       TWI僕ID:        目標裝置的TWI ID
 * @param RegAdd    暫存器地址:      要送往的暫存器地址
 * @param Bytes     位元組數:     待送位元組數
 * @param Data_p    待送資料:     待送資料指標
 * @return  0:  通訊成功
 *          1:  Timeout
 *          4:  SLA錯誤
 */
char TWIM_trm(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p);
/**
 * @brief ASA TWI 收訊函式
 *
 * @param mode      TWI通訊模式:    目前支援:1, 2, 3, 4, 5
 * @param SLA       TWI僕ID:        目標裝置的TWI ID
 * @param RegAdd    暫存器地址:      要送往的暫存器地址
 * @param Bytes     位元組數:       待送位元組數
 * @param Data_p    待讀資料:       待讀資料的地址指標
 * @return  0:  通訊成功
 *          1:  Timeout
 *          4:  SLA錯誤
 */
char TWIM_rec(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p);
/**
 * @brief
 *
 * @param mode      TWI通訊模式:    目前支援:4
 * @param SLA       TWI僕ID:        目標裝置的TWI ID
 * @param RegAdd    暫存器地址:      要送往的暫存器地址
 * @param Mask      資料遮罩:       過濾出目標位元
 * @param Shift     資料位移:       位移指定位元
 * @param Data_p    待送資料:       待送資料指標
 * @return  0:  通訊成功
 *          1:  Timeout
 *          4:  SLA錯誤
 *          5:  mode選擇錯誤
 */
char TWIM_ftm(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p);
/**
 * @brief
 *
 * @param mode      TWI通訊模式:    目前支援:4
 * @param SLA       TWI僕ID:        目標裝置的TWI ID
 * @param RegAdd    暫存器地址:      要送往的暫存器地址
 * @param Mask      資料遮罩:       過濾出目標位元
 * @param Shift     資料位移:       位移指定位元
 * @param Data_p    待讀資料:       待讀資料的地址指標
 * @return  0:  通訊成功
 *          1:  Timeout
 *          4:  SLA錯誤
 *          5:  mode選擇錯誤
 */
char TWIM_frc(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p);

/**
 * @brief 串列埠中斷執行片段
 */
void TWI1_isr(void);
void TWI2_isr(void);
void TWI3_isr(void);
void TWI4_isr(void);
void TWI5_isr(void);
void TWI6_isr(void);
/* Public Section End */
#endif
