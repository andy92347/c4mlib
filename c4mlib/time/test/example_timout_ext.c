#define F_CPU 11059200UL
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"
#include "c4mlib/config/remo_reg.cfg"

void init_ext0();
void init_timer0();

void USER_FUNCTION_1(void* pvData);
void USER_FUNCTION_2(void* pvData);

#define INT_FREQ_DIV_INI \
    { 0 }

/* 全域區塊 */

/* 常數巨集定義區段 */
// 定義可遠端讀寫變數的位元組數巨集替代字串
#define REMOREG_0_SIZE (4)
// 定義可遠端讀寫變數的初始值巨集替代字串
#define REMOREG_0_INI (0)
// 定義逾時中斷次數門檻值巨集替代字串
#define IFD_TIMEOUT_THRESHOLD (100)

/* 全域變數宣告區段 */
// 宣告可遠端讀寫變數
uint32_t remoreg0 = REMOREG_0_INI;
// 宣告外部中斷硬體管理用資料結構
TypeOfExtInt ExtInt0 = EXTINT_0_STR_INI;
// 宣告計時器硬體管理用資料結構
TypeOfTimInt TimInt0 = TIMINT_0_STR_INI;
// 宣告中斷除頻後執行函式管理用資料結構
TypeOfIntFreqDiv IFD0 = INT_FREQ_DIV_INI;
// 宣告串列通訊埠管理用資料結構
TypeOfSerialIsr SerialIsrUart = SERIAL_ISR_STR_UART_INI;

// 宣告儲存中斷除頻後執行工作的工作編號變數
uint8_t IFDFB0Id;

/* 主程式區塊 */
int main() {
    /* Variables 變數宣告及定義區段 */
    // 宣告儲存可遠端讀寫暫存器的地址編號變數
    uint8_t remoreg0AddressId;

    // 宣告儲存外部中斷中執行工作的工作編號變數
    uint8_t EXTFB0Id;

    // 宣告儲存計時中斷中執行工作的工作編號變數
    uint8_t TIMFB0Id;

    /* 函式庫內建及使用者自建初始設定函式區段 */

    // 呼叫ASA_DEVICE_set()執行ASA函式庫之初始化設定。
    ASA_DEVICE_set();

    // 執行外部中斷初始化設定
    init_ext0();

    // 執行計時器初始化設定
    init_timer0();

    // 呼叫ExtInt_init()執行外部中斷硬體管理用資料結構初始化設定
    ExtInt_init(&ExtInt0);
    // 呼叫TimInt_init()執行計時器硬體管理用資料結構初始化設定
    TimInt_init(&TimInt0);
    // 呼叫SerialIsrStr_init()執行串列通訊埠管理用資料結構初始化設定

    // FIXME: 使用新版初始化
    SerialIsr_init(&SerialIsrUart, 0);

    // 呼叫ExtInt_reg()登陸外部中斷中執行工作並取得編號
    EXTFB0Id = ExtInt_reg(&ExtInt0, USER_FUNCTION_1, &remoreg0);

    //呼叫IntFreqDiv_reg() 登陸中斷除頻後執行工作並取得編號，設定為One -
    // shot模式 cycle傳參設0，phase設定為中斷次數門檻值。
    IFDFB0Id = IntFreqDiv_reg(&IFD0, USER_FUNCTION_2, &remoreg0, 0,
                              IFD_TIMEOUT_THRESHOLD);

    // 呼叫TimInt_reg()登陸除頻器功能方塊作為計時中斷中執行工作，並取得編號
    TIMFB0Id = TimInt_reg(&TimInt0, (Func_t)IntFreqDiv_step, &IFD0);

    // 呼叫RemoRW_reg()登陸可遠端讀寫變數並取得編號
    remoreg0AddressId = RemoRW_reg(&SerialIsrUart, &remoreg0, REMOREG_0_SIZE);
    //回傳可遠端讀寫變數登陸結果(發布階段需刪除)
    printf("Create RemoRWreg [%u] with %u bytes \n", remoreg0AddressId,
           SerialIsrUart.remo_reg[remoreg0AddressId].sz_reg);
    printf("EXTFB0Id [%d] is \n", EXTFB0Id);
    printf("TIMFB0Id [%d] is \n", TIMFB0Id);

    // 呼叫sei()開啟全域中斷
    // sei();

    while (1)
        ;  // Block here
}

/* 副函式區段 */
// 外部中斷中執行工作副函式區段
void USER_FUNCTION_1(void* pvData) {
    // 執行使用者定義之指定工作

    // 將結果放入先前宣告之可遠端讀寫變數
    // 重置除頻器功能方塊的計數器
    IFD0.Task[IFDFB0Id].counter = IFD0.Task[IFDFB0Id].phase;
}
// 中斷除頻後執行工作副函式區段
void USER_FUNCTION_2(void* pvData) {
    // 執行使用者定義之指定工作

    // 將結果放入先前宣告之可遠端讀寫變數
}

void init_ext0() {
    // TODO: 完成設定EXT0
}

// Initialize the TIME0 with CTC mode, interrupt at 1000 Hz
void init_timer0() {
    // Pre-scale 128
    // TCCR0 [FOC0 WGM00 COM01 COM00 WGM01 CS02 CS01 CS00]
    // Mode  WGMn1 WGMn0 Mode TOP   TOVn_FlagSet_on
    // 2     1     0     CTC  OCRn  Immediate MAX
    OCR0 = 86 * 2;
    TCCR0 = 0b00001101;
    TCNT0 = 0;
    TIMSK |= 1 << OCIE0;
}
