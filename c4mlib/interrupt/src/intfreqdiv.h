/**
 * @file asa_intfreqdiv.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.1.19
 * @brief
 * 提供ASA函式庫中斷除頻功能，將提供標準step()函式呼叫，根據已設定之cycle與phase，提供條件式觸發呼叫功能。
 */

// TODO: 完成此份文件註解

#ifndef C4MLIB_COMMON_ASA_INTFREQDIV_H
#define C4MLIB_COMMON_ASA_INTFREQDIV_H

#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

typedef struct {
    volatile uint16_t cycle;    // 計數觸發週期
    volatile uint16_t phase;    // 計數觸發相位
    volatile uint16_t counter;  // 計數器 計數器固定頻率上數
    volatile uint8_t postSlot;  // POST欄住址 一指標指向POST欄住址
    volatile uint8_t enable;  // 禁致能控制 決定本逾時ISR是否致能
    volatile Func_t
        func_p;  // 逾時中斷函式 逾時中斷函式指標，為無回傳 void*傳參函式。
    void* funcPara_p;  // 中斷中執行函式專用結構化資料住址指標
} TypeOfIntFreqDivISR;

typedef struct {
    uint8_t total;  // 紀錄已註冊中斷除頻工作
    volatile TypeOfIntFreqDivISR
        Task[MAX_IFD_FuncNum];  // 紀錄所有已註冊的計時中斷
} TypeOfIntFreqDiv;

// TODO: 可用性分析?，由於都是靜態初始值，所以似乎沒有runtime中初始化的需求
// Initiailize the TypeOfIntFreqDiv
void IntFreqDiv_init(TypeOfIntFreqDiv* IFDStr_p);

// Register the IntFreqDiv item into specific IntFreqDiv Manager
uint8_t IntFreqDiv_reg(TypeOfIntFreqDiv* IFDStr_p, Func_t func_p,
                       void* funcPara_p, uint16_t cycle, uint16_t phase);

// Change the enable/Disable configure for specific task object
void IntFreqDiv_en(TypeOfIntFreqDiv* IFDStr_p, uint8_t isr_id, uint8_t enable);

// Compute the one step for specific IntFreqDiv Manger
void IntFreqDiv_step(TypeOfIntFreqDiv* pvData);

#endif  // C4MLIB_COMMON_ASA_INTFREQDIV_H
