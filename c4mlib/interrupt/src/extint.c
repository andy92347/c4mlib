/**
 * @file extint.c
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.04.01
 * @brief
 * 提供ASA函式庫標準外部中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

// TODO: 完成此份文件之註解

/**
 * NOTE
 * ExtInt_step is called by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "extint.h"

/* Library memory mapping table Start */
TypeOfExtInt* RedirectTableOfExtIntStr[MAX_ISR_EXT_NUM] = {0};
/* Library memory mapping table End */

// Initialize the TypeOfTimInt struct and linking to Library's TIM_isr()
void ExtInt_init(TypeOfExtInt* extInt_p) {
    // Makesure the pointer is valid
    if (extInt_p != NULL) {
        RedirectTableOfExtIntStr[extInt_p->EXTI_UID] = extInt_p;
    }
}

// Register EXTI Isr instance
uint8_t ExtInt_reg(TypeOfExtInt* extInt_p, Func_t isrFunc_p,
                   void* isrFuncStr_p) {
    // Makesure the pointer is valid
    if (extInt_p == NULL) {
        return -1;
    }

    uint8_t new_Id = extInt_p->total;
    extInt_p->task[new_Id].enable = 0;  // Default Disable
    extInt_p->task[new_Id].func_p = isrFunc_p;
    extInt_p->task[new_Id].funcPara_p = isrFuncStr_p;
    extInt_p->task[new_Id].postSlot = 0;
    extInt_p->total++;

    return new_Id;
}

// Change the Enable/Disable configure for specific Isr task object
void ExtInt_en(TypeOfExtInt* extInt_p, uint8_t isr_Id, uint8_t enable) {
    // Makesure the pointer is valid
    if (extInt_p == NULL) {
        return;
    }
    extInt_p->task[isr_Id].enable = enable;

    return;
}

// Called by internal ISR, private function
void ExtInt_step(TypeOfExtInt* extInt_p) {
    // Call Each Enable task
    for (uint8_t i = 0; i < extInt_p->total; i++) {
        if (extInt_p->task[i].enable) {
            // TODO: V4.0.0 targert: Modify to Lutos compatiable execute
            // methold. NOTE: postSlot onln On at func_p execute routine!
            extInt_p->task[i].postSlot = 1;
            extInt_p->task[i].func_p(extInt_p->task[i].funcPara_p);
            extInt_p->task[i].postSlot = 0;
        }
    }
}
