#define F_CPU 11059200UL
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

TypeOfTimInt TimInt0 = TIMINT_0_STR_INI;
TypeOfTimInt TimInt1 = TIMINT_1_STR_INI;
TypeOfTimInt TimInt2 = TIMINT_2_STR_INI;
TypeOfTimInt TimInt3 = TIMINT_3_STR_INI;

void init_timer0();
void init_timer1();
void init_timer2();
void init_timer3();

void USER_FUNCTION(void* pvData);

int main() {
    ASA_STDIO_init();
    ASABUS_ID_init();

    /***** Test the  TimInt component *****/
    printf("======= Test TimInt component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    printf("Start timer0 with CTC 1000Hz\n");
    init_timer0();
    printf("Start timer1 with CTC 1000Hz\n");
    init_timer1();
    printf("Start timer2 with CTC 1000Hz\n");
    init_timer2();
    printf("Start timer3 with CTC 1000Hz\n");
    init_timer3();

    // Initialize the connect TypeOfTimInt with Internal TIM_isr();
    //
    TimInt_init(&TimInt0);
    TimInt_init(&TimInt1);
    TimInt_init(&TimInt2);
    TimInt_init(&TimInt3);

    uint8_t fg_interrupt0 = 0;
    uint8_t fg_interrupt1 = 1;
    uint8_t fg_interrupt2 = 2;
    uint8_t fg_interrupt3 = 3;

    //          ISRStr_p  ISRFunc_p      ISRFuncParam_p
    TimInt_reg(&TimInt0, USER_FUNCTION, fg_interrupt0);
    printf("Added USER_FUNCTION with fg_interrupt0  to TimInt0 Component\n");
    TimInt_reg(&TimInt1, USER_FUNCTION, fg_interrupt1);
    printf("Added USER_FUNCTION with fg_interrupt1  to TimInt1 Component\n");
    TimInt_reg(&TimInt2, USER_FUNCTION, fg_interrupt2);
    printf("Added USER_FUNCTION with fg_interrupt2  to TimInt2 Component\n");
    TimInt_reg(&TimInt3, USER_FUNCTION, fg_interrupt3);
    printf("Added USER_FUNCTION with fg_interrupt3  to TimInt3 Component\n");

    printf("Enable Global Interrupt\n");
    sei();

    while (1)
        ;  // Block here
}

// Initialize the TIME0 with CTC mode, interrupt at 1000 Hz
void init_timer0() {
    // Pre-scale 128
    // TCCR0 [FOC0 WGM00 COM01 COM00 WGM01 CS02 CS01 CS00]
    // Mode  WGMn1 WGMn0 Mode TOP   TOVn_FlagSet_on
    // 2     1     0     CTC  OCRn  Immediate MAX
    OCR0 = 86 * 2;
    TCCR0 = 0b00001101;
    TCNT0 = 0;
    TIMSK |= 1 << OCIE0;
}

// Initialize the TIME1 with CTC mode, interrupt at 1000 Hz
void init_timer1() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR1A  [COM1A1 COM1A0 COM1B1 COM1B0 COM1C1 COM3C0 WGM11 WGM10]
    // TCCR1B  [ICNC1 ICES1 �V WGM13 WGM12 CS12 CS11 CS10]

    ICR3 = 11058 * 2;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00011001;
    TCNT1 = 0x00;
    TCNT1 = 0;
    TIMSK |= 1 << OCIE1A;
}

// Initialize the TIME2 with CTC mode, interrupt at 1000 Hz
void init_timer2() {
    // Pre-scale 128
    // TCCR2 [FOC2 WGM20 COM21 COM20 WGM21 CS22 CS21 CS20]
    // Mode  WGMn1 WGMn0 Mode TOP   TOVn_FlagSet_on
    // 2     1     0     CTC  OCRn  Immediate MAX
    OCR2 = 86 * 2;
    TCCR2 = 0b00001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer3() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0;
    ETIMSK |= 1 << OCIE3A;
}

void USER_FUNCTION(void* pvData) {
    uint8_t* fg_interrupt = (uint8_t*)pvData;

    if (fg_interrupt != NULL) {
        switch (*fg_interrupt) {
            case 0:
                PORTA ^= 1;
                break;
            case 1:
                PORTA ^= 2;
                break;
            case 2:
                PORTA ^= 4;
                break;
            case 3:
                PORTA ^= 8;
                break;
        }
    }
}
