#define F_CPU 11059200UL
#define CFbit 4  // 假設 CF 占了 4 bits

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/common/src/common.h"
#include "c4mlib/common/src/hal_time.h"
#include "c4mlib/hardware/src/isr.h"

#include <util/delay.h>

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    ASA_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    uint8_t data1_buffer[5] = {5, 6, 7, 8, 9};
    uint8_t data2_buffer[5] = {0, 0, 0, 0, 0};
    uint8_t CF = 0x01;
    uint8_t result = 0;

    /*
     * Mode1 先低後高
     * Data+CF (CF向右對齊 Data向左對齊)
     */

    /***** UARTM Mode1 Transmit test *****/
    /*** 【test 1】 ***/
    printf("【test 1】傳送 4 筆資料\n");
    CF = CF >> 0;
    data1_buffer[0] = data1_buffer[0] << CFbit;
    result = UARTM_trm(1, 0, CF, 4, data1_buffer);
    if (result)
        printf("<1>UARTM_trm Fail [%u]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 1】UARTM_trm : data[%u]=%u\n", i, data1_buffer[i]);
    }
    HAL_delay(100);

    /***** UARTM Mode1  Receive test *****/
    result = UARTM_rec(1, 0, CF, 5, data2_buffer);
    if (result)
        printf("<1>UARTM_rec Fail [%u]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 2】UARTM_rec : data[%u]=%u\n", i, data2_buffer[i]);
    }
    while (1) {
    }  // Block here
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
